import { Component, OnChanges, OnInit } from '@angular/core';
import { DetailsService } from '../shared/details.service';
import { IUser } from '../shared/user';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent {
  users: IUser[];

  constructor(private detailsService: DetailsService) {
    this.users = [];
    this.detailsService.getUsers().subscribe({
      next: (users) => (this.users = users)
    });
  }

}
