import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';
import { DetailsService } from '../shared/details.service';
import { IUser } from '../shared/user';

@Component({
  selector: 'app-details-editor',
  templateUrl: './details-editor.component.html',
  styleUrls: ['./details-editor.component.scss']
})
export class DetailsEditorComponent {

  constructor(private detailsService: DetailsService) { }

  name = new FormControl('');
  lastname = new FormControl('');
  age = new FormControl('');
  gender = new FormControl('');
  count: number = 0;

  addToList(){
    let [month, day, year] = new Date().toLocaleDateString("en-US").split("/");
    let user: IUser = {
      id: `${this.count++}`,
      name: `${this.name.value}`,
      lastname: `${this.lastname.value}`,
      age: `${this.age.value}`,
      gender: `${this.gender.value}`,
      creationDate: `${year}-${day}-${month}`
    }
    let response: IUser[] = [];
    this.detailsService.getUsers().subscribe({
      next: (users) => (response = users)
    });
    this.detailsService.setUsers([...response, user ]);
  }

}
