import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { IUser } from './user';

@Injectable({
  providedIn: 'root'
})
export class DetailsService {
  count: number = 0;
  private _users: BehaviorSubject<IUser[]>;
  constructor() { 
    this._users = new BehaviorSubject<IUser[]>([]);
  }

  getUsers(): Observable<IUser[]> {
    return this._users.asObservable();
  }
  setUsers(value: IUser[]) {
    this._users.next(value);
  }
}
