export interface IUser {
    id: string,
    name: string,
    lastname: string,
    age: string,
    gender: string,
    creationDate: string
}